﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace RocketCore
{
    class FuncCall
    {
        private static long next_id; //в целях автоинкремента id вызова функции ядра
        internal long FuncCallId { get; private set; }
        internal Action Action { get; set; }

        internal FuncCall() //конструктор вызова функции
        {
            FuncCallId = Interlocked.Increment(ref next_id); //инкремент id предыдущего вызова
        }
    }
}
